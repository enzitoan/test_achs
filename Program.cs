﻿using System;

namespace TestAchs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to test ACHS!!");
            int staircaseSize = 0;           
            
            Console.WriteLine("Insert size of staircase: ");
            staircaseSize = Convert.ToInt32(Console.ReadLine());

            var test = new Staircase();
            test.PrintStaircase(staircaseSize);
        }

    }
}
