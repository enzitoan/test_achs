using System;
using System.Text;

namespace TestAchs
{
   public class Staircase
   {

    public void PrintStaircase(int size)
    {
        if (size < 1 || size > 100)
        {
            Console.WriteLine("size out of range");
            return;
        }

        StringBuilder sb  = new StringBuilder();

        for (var i = 1; i <= size; i++)
        {
            sb.AppendLine(GetStep(size, i));
        }       

        Console.WriteLine(sb.ToString());
    }

    private string GetStep(int size, int step) 
    {
        return string.Concat(new String(' ', size - step), new String('#', step));
    } 

   } 
}
