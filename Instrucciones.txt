Instrucciones

INGLES
-------------------------------------------------------------

Consider a staircase of size N=4:
   #
  ##
 ###
####
Observe that its base and height are both equal to “n”, and the image is drawn using # symbols and spaces. The last line is not preceded by any spaces.

Write a program that prints a staircase of size “n”.

Function Description

Complete the challenge printing a staircase as described above.

staircase has the following parameter(s):

n: an integer

Input Format

A single integer,”n”, denoting the size of the staircase.

Constraints

0<n<=100.

Output Format

Print a staircase of size “n” using # symbols and spaces.

Note: The last line must have0 spaces in it.


Sample Input

6 
Sample Output

     #
    ##
   ###
  ####
 #####
######
Explanation

The staircase is right-aligned, composed of# symbols and spaces, and has a height and width of n=6.

ESPAÑOL
------------------------------------------

Considere una escalera de tamaño N = 4:
   #
  ##
 ###
####
Observe que su base y altura son iguales a “n”, y la imagen se dibuja usando símbolos # y espacios. La última línea no está precedida por espacios.

Escribe un programa que imprima una escalera de tamaño “n”.

Función descriptiva

Completa el desafío imprimiendo una escalera como se describe arriba.

escalera tiene los siguientes parámetros:

n: un número entero

Formato de entrada

Un solo entero, "n", que indica el tamaño de la escalera.

Restricciones

0 <n <= 100.

Formato de salida

Imprima una escalera de tamaño “n” usando # símbolos y espacios.

Nota: La última línea debe tener 0 espacios.


Entrada de muestra

6
Salida de muestra

     #
    ##
   ###
  ####
 #####
######
Explicación

La escalera está alineada a la derecha, compuesta por # símbolos y espacios, y tiene una altura y un ancho de n = 6.